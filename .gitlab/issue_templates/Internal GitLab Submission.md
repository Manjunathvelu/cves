### Vulnerability Submission

#### Publishing Schedule

After a CVE request is validated, a CVE identifier will be assigned. On what
schedule should the details of the CVE be published? This may be updated at any time and signal when an advisory is ready to publish.

* [ ] Publish immediately
* [x] Wait to publish

#### Request CVE ID 
On initial submission, you may automatically request a CVE ID. Be aware, this will not work after the initial submission.

* [x] Automatically assign CVE ID
<!--
Please fill out the yaml codeblock below
-->

```yaml
reporter:
  name: "GitLab Security Team" # "First Last"
  email: "security@gitlab.com" # "email@domain.tld"
vulnerability:
  # If the affected product is GitLab, please mention if it's GitLab CE/EE or GitLab EE
  description: "TODO" # "[VULNTYPE] in [COMPONENT] in [VENDOR][PRODUCT] affecting all versions from 10.0 prior to 12.9.8, 12.10 prior to 12.10.7, and 13.0 prior to 13.0.1 allows [ATTACKER] to [IMPACT] via [VECTOR]"
  cwe: "CWE-0" # TODO, for example "CWE-22" # Path Traversal
  product:
    gitlab_path: "gitlab-org/gitlab" # "namespace/project" # the path of the project within gitlab
    vendor: "GitLab" # "iTerm2"
    name: "GitLab" # "iTerm2"
    affected_versions:
      - ">=10.0, <12.9.8" # "1.2.3"
      - ">=12.10, <12.10.7" # "1.2.3"
      - ">=13.0, <13.0.1" # "1.2.3"
    fixed_versions:
      - "13.0.1" # "1.2.4"
      - "12.10.7" # "1.3.10
      - "12.9.8" # "1.3.10
  impact: "AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:N" # CVSS:3.1 string, without the prefix
  solution: "Upgrade to version 13.0.1, 12.10.7 or 12.9.8" # "Upgrade to version 1.2.4 or 1.3.10"
  credit: "Thanks [<ENTER REPORTER NAME>](https://hackerone.com/<ENTER REPORTER NAME>) for reporting this vulnerability through our HackerOne bug bounty program"
  # credit: "This vulnerability has been discovered internally by the GitLab team"
  references:
    - "https://gitlab.com/gitlab-org/gitlab/-/issues/[ENTER ISSUE]"
    - "https://hackerone.com/reports/[ENTER REPORT IF APPLICABLE]"
```


CVSS scores can be computed by means of the [GitLab CVSS Calculator](https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/) (see also the [CVSS Calculation Guide](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/cvss-calculation.html)).

Alternatively, you can use the [NVD CVSS Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator).

/label ~"devops::secure" ~"group::vulnerability research" ~"vulnerability research::cve" ~"advisory::queued"
/confidential
